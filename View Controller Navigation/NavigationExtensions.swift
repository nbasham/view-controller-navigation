import UIKit

extension UIViewController {

    public typealias ConfigureCallback<T> = (_ vc: T) -> Swift.Void

    @discardableResult public func push<T: UIViewController>(_ name: String, configure: ConfigureCallback<T>?) -> T {
        return _present(.push, name: name, configure: configure)
    }

    @discardableResult public func presentModal<T: UIViewController>(_ name: String, configure: ConfigureCallback<T>?) -> T {
        return _present(.modal, name: name, configure: configure)
    }

    @discardableResult public func presentPopover<T: UIViewController>(_ name: String, anchor: Any?, configure: ConfigureCallback<T>?) -> T {
        return _present(.popover, name: name, anchor: anchor, configure: configure)
    }

    enum StoryboardReturnType {

        case viewController(viewController: UIViewController)
        case navigationController(navigationController: UINavigationController)

        func asViewController() -> UIViewController {
            switch self {
            case let .viewController(viewController):
                return viewController
            case let .navigationController(navigationController):
                return navigationController.topViewController!
            }
        }

        func asNavigationController() -> UINavigationController? {
            switch self {
            case .viewController(_):
                return nil
            case .navigationController(let navigationController):
                return navigationController
            }
        }

        func root() -> UIViewController {
            switch self {
            case .viewController(let viewController):
                return viewController
            case .navigationController(let navigationController):
                return navigationController
            }
        }
    }

    private enum NavigationType {
        case push, modal, popover
    }

    @discardableResult private func _present<T: UIViewController>(_ type: NavigationType, name: String, anchor: Any? = nil, configure: ConfigureCallback<T>?) -> T {
        let rt = loadStoryboard(name)
        guard let vc = rt.asViewController() as? T, let nc = navigationController else { fatalError() }
        if case .popover = type {
            configurePopover(vc, delegate: vc, anchor: anchor)
        }
        applyConfig(vc, configure: configure)
        switch type {
        case .push:
            nc.pushViewController(vc, animated: true)
        case .modal:
            nc.present(rt.root(), animated: true, completion: nil)
        case .popover:
            nc.present(rt.root(), animated: true, completion: nil)
        }
        return vc
    }

    func loadStoryboard(_ name: String) -> StoryboardReturnType {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let either = storyboard.instantiateInitialViewController()
        if let nc = either as? UINavigationController {
            return .navigationController(navigationController: nc)
        } else if let vc = either {
            return .viewController(viewController: vc)
        } else {
            fatalError(
                """


                    --- Storyboard must have Is Initial View Controller option set to on. ---


                    """
            )
        }
    }

    func applyConfig<T: UIViewController>(_ vc: T, configure: ConfigureCallback<T>?) {
        if let configure = configure {
            vc.loadViewIfNeeded()
            configure(vc)
        }
    }

    private func configurePopover(_ vc: UIViewController, delegate: Any?, anchor: Any?) {
        guard let delegate = vc as? UIPopoverPresentationControllerDelegate else {
            fatalError("""


        --- A popover view controller must derive from PopoverViewController OR implement: ---

        extension VC: UIPopoverPresentationControllerDelegate {

        public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
        }

        public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
        }
        }

        """ )
        }
        guard anchor != nil else { fatalError("Must include an anchor.")}
        vc.modalPresentationStyle = .popover
        vc.popoverPresentationController?.delegate = delegate
        if let barButtonItem = anchor as? UIBarButtonItem {
            vc.popoverPresentationController?.barButtonItem = barButtonItem
        } else if let sourceView = anchor as? UIView {
            vc.popoverPresentationController?.sourceView = sourceView
            vc.popoverPresentationController?.sourceRect = sourceView.bounds
        }
    }
}

extension UIViewController {
    
    func applyDismissActionToRightBarButtonItem() {
        self.navigationItem.rightBarButtonItem?.action = #selector(self.dismissViewController as () -> Void)
        self.navigationItem.rightBarButtonItem?.target = self
    }

    @objc func dismissViewController() {
        self.dismiss(animated: true, completion: nil)
    }
}

class PopoverViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

class PopoverTableViewController: UITableViewController, UIPopoverPresentationControllerDelegate {

    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

