import UIKit

class MainViewController: UIViewController {

    var isChildVisible = false // for toggling child view controller

    @IBAction func yellow(_ sender: Any) {
        push("Yellow") { (vc: YellowViewController) in
            vc.label.text = "1 view controller, 1 storyboard, it's how God meant it to be"
        }
    }

    @IBAction func orange(_ sender: Any) {
        presentModal("Orange") { (vc: OrangeViewController) in
            vc.applyDismissActionToRightBarButtonItem()
        }
    }

    @IBAction func green(_ sender: Any) {
        presentPopover("Green", anchor: sender) { (vc: GreenViewController) in
            vc.preferredContentSize = CGSize(width: 150, height: 180)
            vc.popoverPresentationController?.permittedArrowDirections = .left
        }
    }

}

extension MainViewController { // AncillaryNavigationExtensions

    @IBAction func purple(_ sender: Any) {
        if isChildVisible {
            removeChild("Purple")
        } else {
            addChild("Purple") { (vc: PurpleViewController) in
                vc.view.frame = CGRect(x: 180, y: 200, width: 100, height: 100)
            }
        }
        isChildVisible = !isChildVisible
    }

    @IBAction func alert(_ sender: Any) {
        okAlert(title: "Segues", message: "Say goodbye!")
    }

    @IBAction func email(_ sender: Any) {
        email(address: "puzzlepleasure@gmail.com", subject: "Daily Puzzles")
    }
}
