import UIKit
import MessageUI

extension UIViewController {
    public func okAlert(title: String? = nil, message: String? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (result : UIAlertAction) -> Void in }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension UIViewController {
    @discardableResult public func addChild<T: UIViewController>(_ name: String, configure: ConfigureCallback<T>?) -> T {
        let rt = loadStoryboard(name)
        guard let vc = rt.asViewController() as? T else { fatalError() }
        addChildViewController(vc)
        applyConfig(vc, configure: configure)
        self.view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
        return vc
    }
    public func removeChild(_ name: String) {
        guard let vc = findChildViewController(name) else { return }
        vc.willMove(toParentViewController: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParentViewController()
    }
    private func findChildViewController(_ name: String) -> UIViewController? {
        guard var nameSpace = Bundle.main.infoDictionary!["CFBundleExecutable"] as? String else { return nil }
        nameSpace = nameSpace.replacingOccurrences(of: " ", with: "_")
        guard let claz = NSClassFromString("\(nameSpace).\(name)ViewController") else { return nil }
        for vc in childViewControllers {
            if vc.isKind(of: claz) {
                return vc
            }
        }
        return nil
    }
}

extension UIViewController: MFMailComposeViewControllerDelegate {
    public func email(address: String, subject: String) {
        if MFMailComposeViewController.canSendMail() {
            let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            let appName = Bundle.main.infoDictionary?["CFBundleName"] as! String
            let subject = "\(subject) - \(appName) \(version) iOS: \(UIDevice.current.systemVersion)"
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([address])
            mail.setSubject(subject)
            self.present(mail, animated: true)
        } else {
            okAlert(title: "This device is not setup to send email yet.")
        }
    }
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

